---
title:  Hugo Photoswipe Gallery Bug 
subtitle: How to fix the Hugo Photoswipe Gallery Bug
date: 2018-12-19
tags: ["hugo", "photoswipe", "gitlab"]
---

{{< gallery caption-effect="fade" >}}
{{< figure thumb="-thumb" link="/img/hexagon.jpg" >}}
{{< figure thumb="-thumb" link="/img/sphere.jpg" caption="Sphere" >}}
{{< figure thumb="-thumb" link="/img/triangle.jpg" caption="Triangle" alt="This is a long comment about a triangle" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

<!--more-->

### The Problem
There is the [Photoswipe Gallery Sample](https://pages.gitlab.io/hugo/post/2017-03-20-photoswipe-gallery-sample/) in the [Hugo Theme](https://pages.gitlab.io/hugo/)for GitLab pages, if You clone it from [GitLab](https://gitlab.com).  
But there is a bug, inside a post with Photoswipe Gallery, where u get stuck after clicking on an image and closing it  

Thanks to [this](https://github.com/liwenyip/hugo-easy-gallery/issues/6) article I understood what went wrong.  
So basicly its because in the _load-photoswipe.js_ file the last line triggeres Photoswipe twice.
So [Li-Wen Yip](https://www.liwen.id.au/heg/) the creator stated:  

> The main point is: if you are going to load the js files in the footer of your theme, you need to comment 
> out the _< script>_ tags in the _load-photoswipe shortcode_ (or use the _load-photoswipe-theme_ shortcode).

Also he pointed to this [instruction page](https://github.com/liwenyip/hugo-easy-gallery#theme-integration).

However, I could not solve the problem with ether commenting out any kind of _< script>_ tags or even delting the whole code part inside the file _*hugo/themes/beautifulhugo/layouts/shortcodes/load-photoswipe.html*_:

```
#<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
#<script src="/js/load-photoswipe.js"></script>
```
It would only destroy the the Photoswipe Gallery in the main page but inside of a post the Photoswipe Gallery was still working.  
Also using ***load-photoswipe-theme*** instead of ***load-photoswipe*** didn`t solve the problem for me.
It destroyed Photoswipe Gallery inside the post.  
So i thougt i could disable the Photoswipe shortcode in the footer since it runs already in the header and the problem is caused by running it twice.  
By removing the following code part from the file ***hugo/themes/beautifulhugo/layouts/partials/footer.html***
```
<!-- Load PhotoSwipe js if the load-photoswipe shortcode has been used -->
{{ if ($.Scratch.Get "photoswipeloaded") }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe.min.js" integrity="sha256-UplRCs9v4KXVJvVY+p+RSo5Q4ilAUXh7kpjyIP5odyc=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe-ui-default.min.js" integrity="sha256-PWHOlUzc96pMc8ThwRIXPn8yH4NOLu42RQ0b9SpnpFk=" crossorigin="anonymous"></script>
<script src="/js/load-photoswipe.js"></script>
{{ end }}
```
Between this two lines:
```
<script> renderMathInElement(document.body); </script>

<!-- Google Custom Search Engine -->
```


So after that the file looks like this :
```
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <ul class="list-inline text-center footer-links">
          {{ range .Site.Data.beautifulhugo.social.social_icons }}
            {{- if isset $.Site.Author .id }}
              <li>
                <a href="{{ printf .url (index $.Site.Author .id) }}" title="{{ .title }}">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="{{ .icon }} fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
            {{- end -}}
          {{ end }}
          {{ if .Site.Params.rss }}
          <li>
            <a href="{{ "index.xml" | absLangURL }}" title="RSS">
              <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-rss fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          {{ end }}
        </ul>
        <p class="credits copyright text-muted">
          {{ .Site.Author.name }}
          &nbsp;&bull;&nbsp;
          {{ .Site.LastChange.Format "2006" }}

          {{ if .Site.Title }}
            &nbsp;&bull;&nbsp;
            <a href="{{ "" | absLangURL }}">{{ .Site.Title }}</a>
          {{ end }}
        </p>
        <!-- Please don't remove this, keep my open source work credited :) -->
        <p class="credits theme-by text-muted">
          {{ i18n "poweredBy" . | safeHTML }}
          {{ with .Site.Params.commit }}&nbsp;&bull;&nbsp;[<a href="{{.}}{{ getenv "GIT_COMMIT_SHA" }}">{{ getenv "GIT_COMMIT_SHA_SHORT" }}</a>]{{ end }}
        </p>
      </div>
    </div>
  </div>
</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.7.1/katex.min.js" integrity="sha384-/y1Nn9+QQAipbNQWU65krzJralCnuOasHncUFXGkdwntGeSvQicrYkiUBwsgUqc1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.7.1/contrib/auto-render.min.js" integrity="sha384-dq1/gEHSxPZQ7DdrM82ID4YVol9BYyU7GbWlIwnwyPzotpoc57wDw/guX8EaYGPx" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ "js/main.js" | absURL }}"></script>
<script src="{{ "js/highlight.min.js" | absURL }}"></script>
<script> hljs.initHighlightingOnLoad(); </script>
<script> renderMathInElement(document.body); </script>

<!-- Google Custom Search Engine -->
{{ if .Site.Params.gcse }}
<script>
  (function() {
    var cx = '{{ .Site.Params.gcse }}';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
{{ end }}

{{- partial "footer_custom.html" . }}

```
And voilà it worked, The Photoswipe Gallery now works on the main page and inside of a post.