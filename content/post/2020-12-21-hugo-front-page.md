---
title:  Beautiful Hugo - no posts on front page
subtitle: posts not showing on frontpage
date: 2020-12-21
tags: ["hugo", "gitlab"]
---
![hugo](/img/hugo.jpg)
<!--more-->

### Change the index.html:


You can resolve this by changing /themes/beautifulhugo/layouts/index.html line 12 to:

```
{{ $pag := .Paginate (where .Site.RegularPages "Type" "post") }} 
```
This automatically loaded all the published posts beneath the header.

