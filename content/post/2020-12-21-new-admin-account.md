---
title:  macOS - creating new Admin Account 
subtitle: Forgot your Mac Password - create a new Admin Account
date: 2020-12-21
tags: ["Mac", "macOS",]
---
![macwelcome](/img/macwelcome.jpg)
<!--more-->

### Create the new admin account:
If you forgot the Password on your Mac, the best sollution is createing a new admin account:

* Boot into Single User Mode by pressing ```⌘ + S``` before you hear the Apple chime.
* Mount the drive by typing ```/sbin/mount –uw / ``` then enter.
* Remove the Apple Setup Done file by typing ```rm -v /var/db/.AppleSetupDone ```then enter.
* Reboot by typing ```reboot``` then enter.
* Complete the setup process, creating a new admin account.

This will force macOS to redo the initial first account creation, and doing so will not affect the current user profiles (they will remain intact) – so, if you prefer to make them as admin later, you can do that as well by logging in using the newly created admin account, then go to System Preferences, then Users & Groups, select the existing user, and tick “Allow user to administer this computer.”
