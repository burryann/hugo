---
title: Pocket C.H.I.P
date: 2018-12-21
---

![Pocket Chip won`t start](https://i.redd.it/ymxr7r5q6cq11.jpg)

<!--more-->

Since the company Next Thing Co is in bakruptcy it is harder to find something for your C.H.I.P or your Pocket C.H.I.P.
It`s still possible, like on reddit: [r/ChipCommunity](https://www.reddit.com/r/ChipCommunity/)

So for this particular Problem i found [this](https://www.reddit.com/r/ChipCommunity/comments/9lksb4/i_need_help/) thread.
So you just need to download the file [config.json](https://pastebin.com/0ZWtjeqy) and upload it to your Pocket C.H.I.P in the .pocket-home folder via ftp.

If u need  all the "lost" infos of NTC u can download all from this [archiv](https://archive.org/details/NextThingCo.WebArchive).
