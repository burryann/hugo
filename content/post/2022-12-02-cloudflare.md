---
title:  Cloudflare - DNS redirect
subtitle: redirect www subdomain traffic to root domain
date: 2022-12-02
tags: ["Cloudflare", "DNS"]
---

![Cloudflare](https://upload.wikimedia.org/wikipedia/commons/4/4b/Cloudflare_Logo.svg)
<!--more-->

#### Create a www subdomain:
Create like (www.example.com) that simply redirects traffic to your root domain (example.com).

```javascript
websites -> your website -> DNS
```

Create a **proxied DNS A record for your** subdomain. This record can point to any IP address since all traffic will be redirected prior to reaching the address.

|Type | Name |IPv4 address| Proxy status    |
|-----|--------|--------|--------|
|A    |www       |192.0.0.1      | Proxied|


#### Create a Bulk Redirect List

* Log in to the Cloudflare dashboardOpen external link and select your account.
* (Optional) In the new dashboard navigation, expand Manage Account.
* Go to Configurations > Lists.
* Click Create new list.
* Enter a list name and description, and select Redirect as the content type.
* Click Create.


#### Add the URL Redirects manually
​​Use **Bulk redirects** to forward traffic from your subdomain to your root domain. You will likely want to include **Subpath matching** and **Preserve path suffix** to ensure requests to *www.example.com/examples* go to *example.com/examples*.
​​

* Under **Add items to list**, enter the URL Redirects you wish to add to the list.		
<br>You must enter at least the following three fields: Source URL, Target URL, and Status. To set additional options, expand Edit Parameters.

* Add more URL Redirects, if required.
* Click Add to list.
​​

|Source URL | Target URL |Status	Selected| parameters |
|-----|--------|--------|--------|
|example.com   |https://www.example.com       |301	     | Subpath matching and Preserve path suffix|

​​
​​
#### Create a Bulk Redirect Rule to enable the redirects in the list

* Go to **Account Home > Bulk Redirects**.
* Click **Create Bulk Redirects**.
* Enter a rule description.
* Select the Bulk Redirect List you previously created.
* (Optional) If necessary, edit the rule expression or the list key.
* To save and deploy the Bulk Redirect Rule, click **Save and Deploy**.

[https://developers.cloudflare.com/fundamentals/get-started/basic-tasks/manage-subdomains/](https://developers.cloudflare.com/fundamentals/get-started/basic-tasks/manage-subdomains/)

[https://developers.cloudflare.com/rules/url-forwarding/bulk-redirects/create-dashboard/](https://developers.cloudflare.com/rules/url-forwarding/bulk-redirects/create-dashboard/)
